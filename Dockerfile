# Utiliser l'image de base Node.js
FROM node:16-alpine

# Définir le répertoire de travail
WORKDIR /app

# Copier les fichiers nécessaires dans l'image
COPY package*.json ./
COPY . .

# Installer les dépendances
RUN npm install

RUN npm install json2video-sdk

# Construire l'application
RUN npm run build

# Exposer le port 3000
EXPOSE 3000

# Lancer l'application
CMD [ "npm", "start" ]