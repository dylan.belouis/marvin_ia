import Link from "next/link";
import Navbar from "../../components/shared-components/Navbar";
import styles from "../../styles/profil.module.css";
import ProfilTemplate from "../../components/profil/ProfilTemplate";

const Profil = () => {
  return (
    <div className={styles.main}>
      <Navbar page="profil" />

      <main>
        <ProfilTemplate />
      </main>
    </div>
  );
};

export default Profil;
