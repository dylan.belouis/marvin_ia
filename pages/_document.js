import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html
      style={{
        margin: "0",
        width: "100vw",
      }}
    >
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Edu+QLD+Beginner:wght@500&family=Inter:wght@200&family=Rubik+Moonrocks&display=swap"
          rel="stylesheet"
        />
        <title>MySocialIA</title>
        <link rel="icon" href="/mysocialIALogo.png" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1"
        />
      </Head>
      <body
        style={{
          margin: 0,
          backgroundColor: "#FAFAFA",
          fontFamily: "Inter, sans-serif",
          width: "100vw",
          minWidth: "100vw",
        }}
      >
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
