import { useState, useEffect } from "react";
import ChatTemplate from "../../components/message/ChatTemplate";
import styles from "../../styles/index.module.css";
import { useRouter } from "next/router";
import Navbar from "../../components/shared-components/Navbar";

const Message: Function = () => {
  const { id } = useRouter().query;
  console.log(id);

  useEffect(() => {}, [id]);

  return (
    <div className={styles.mainMessage}>
      <Navbar page="messages" />
      <ChatTemplate id={id} />
    </div>
  );
};

export default Message;
