import Head from "next/head";
import styles from "../styles/userlist_page.module.css";
import UserListContent from "../components/message/UserList";
import Navbar from "../components/shared-components/Navbar";

const UserList: Function = () => {
  return (
    <div>
      <Head>
        <title>MySocialIA</title>
        <link rel="icon" href="/mysocialIALogo.png" />
      </Head>
      <main className={styles.main}>
        <div className={styles.UserList_navbar}>
          <Navbar page="userlist" />
        </div>
        <UserListContent />
      </main>
    </div>
  );
};

export default UserList;
