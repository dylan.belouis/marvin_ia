import Head from "next/head";
import { useState, useEffect } from "react";
import styles from "../styles/index.module.css";
import Navbar from "../components/shared-components/Navbar";
import Wall from "../components/wall/Wall";
import LoaderLogo from "../components/shared-components/LoaderLogo";

const Home: Function = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    setTimeout(() => setIsLoading(true), 1500);
  }, []);

  return (
    <div className={styles.mainWall}>
      <Head>
        <title>MySocialIA</title>
        <link rel="icon" href="/mysocialIALogo.png" />
      </Head>
      <main className={styles.main}>
        <Navbar page="home" />
        {isLoading ? <Wall /> : <LoaderLogo />}
      </main>
    </div>
  );
};

export default Home;
