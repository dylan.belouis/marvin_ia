const { Configuration, OpenAIApi } = require("openai");

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);

export default async function (req, res) {
  const response = await openai.createCompletion({
    model: "text-davinci-002",
    prompt: `Translate this into 1. French, 2. Spanish and 3. Japanese:\n\n${req.body.message}\n\n1.`,
    temperature: 0.3,
    max_tokens: 100,
    top_p: 1.0,
    frequency_penalty: 0.0,
    presence_penalty: 0.0,
  });
  return console.log(response, "response");
  //   res.status(200).json({ result: response.data.choices[0].text });
  //   res.status(500).json({ result: "Probleme avec l'api" });
}
