const deepai = require("deepai"); // OR include deepai.min.js as a script tag in your HTML

deepai.setApiKey(process.env.DEEPAI_API_KEY);

export default async function (req, res) {
  const resp = await deepai.callStandardApi("text2img", {
    text: req.body.message,
  });
  return res.status(200).json({ result: resp.output_url ?? "non" });
}
