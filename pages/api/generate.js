import { Configuration, OpenAIApi } from "openai";

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);

export default async function (req, res) {
  const completion = await openai.createCompletion("text-davinci-001", {
    prompt: generatePromptMessage(
      req.body.message,
      req.body.prompt,
      req.body.dataIndex
    ),
    temperature: req.body.dataIndex.temperature
      ? req.body.dataIndex.temperature
      : 0,
    max_tokens: req.body.dataIndex.max_tokens
      ? req.body.dataIndex.max_tokens
      : 130,
    top_p: req.body.dataIndex.top_p ? req.body.dataIndex.top_p : 1.0,
    frequency_penalty: req.body.dataIndex.frequency_penalty
      ? req.body.dataIndex.frequency_penalty
      : 0.0,
    presence_penalty: req.body.dataIndex.presence_penalty
      ? req.body.dataIndex.presence_penalty
      : 0.0,
    stop: req.body.dataIndex.stop ? req.body.dataIndex.stop : ['"""'],
  });
  res.status(200).json({ result: completion.data.choices[0].text });
}

function generatePromptMessage(message, prompt, dataIndex) {
  return `${prompt} ${message} ${dataIndex.messageName}`;
}
