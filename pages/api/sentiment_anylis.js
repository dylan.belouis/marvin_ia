const deepai = require("deepai"); // OR include deepai.min.js as a script tag in your HTML

deepai.setApiKey(process.env.DEEPAI_API_KEY);

export default async function (req, res) {
  const result = await deepai.callStandardApi("sentiment-analysis", {
    text: req.body.message,
  });
  return res.status(200).json({ result: result.output[0] });
}
