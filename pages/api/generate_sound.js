const say = require("say");

let speed = 5.0;

export default async function (req, res) {
  say.speak(
    req.body.message.normalize("NFD").replace(/[\u0300-\u036f]/g, ""),
    speed,
    (error) => {
      if (error) {
        return console.error("Error speaking!", error);
      }
    }
  );
}
