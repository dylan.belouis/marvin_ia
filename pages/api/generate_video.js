// import { Configuration, OpenAIApi } from "openai";

// const configuration = new Configuration({
//  apiKey: process.env.OPENAI_API_KEY,
// });
// const openai = new OpenAIApi(configuration);

// export default async function (req, res) {
//  const completion = await openai.createCompletion("text-davinci-001", {
//    prompt: generatePromptMessage(
//      req.body.message,
//      req.body.prompt,
//      req.body.dataIndex
//    ),
//    temperature: req.body.dataIndex.temperature
//      ? req.body.dataIndex.temperature
//      : 0,
//    max_tokens: req.body.dataIndex.max_tokens
//      ? req.body.dataIndex.max_tokens
//      : 130,
//    top_p: req.body.dataIndex.top_p ? req.body.dataIndex.top_p : 1.0,
//    frequency_penalty: req.body.dataIndex.frequency_penalty
//      ? req.body.dataIndex.frequency_penalty
//      : 0.0,
//    presence_penalty: req.body.dataIndex.presence_penalty
//      ? req.body.dataIndex.presence_penalty
//      : 0.0,
//    stop: req.body.dataIndex.stop ? req.body.dataIndex.stop : ['"""'],
//  });
//  res.status(200).json({ result: completion.data.choices[0].text });
// }

// function generatePromptMessage(message, prompt, dataIndex) {
// return `${prompt} ${message} ${dataIndex.messageName}`;
// }

const { Movie, Scene } = require("json2video-sdk");

async function main(req, res) {
  // Create a new movie
  let movie = new Movie();

  // Set your API key
  // Get your free API key at https://json2video.com
  movie.setAPIKey(process.env.JSON2VIDEO_API_KEY);

  // Set movie quality: low, medium, high
  movie.set("quality", "high");

  // Generate a video draft
  movie.set("draft", true);

  // Create a new scene
  let scene = new Scene();

  // Set the scene background color
  scene.set("background-color", "green");

  // Add a text element printing "Hello world" in a fancy way (style 003)
  // The element is 10 seconds long and starts 2 seconds from the scene start
  scene.addElement({
    type: "text",
    style: "003",
    text: "salut tout le monde ca va ?",
    duration: 10,
    start: 2,
  });

  // Add the scene to the movie
  movie.addScene(scene);

  // Call the API and render the movie
  let render = await movie.render();
  console.log(render);

  // Wait for the movie to finish rendering
  await movie
    .waitToFinish((status) => {
      console.log(
        "Rendering: ",
        status.movie.status,
        " / ",
        status.movie.message
      );
    })
    .then((status) => {
      console.log("Movie is ready: ", status.movie.url);
      console.log("Remaining final movies: ", status.remaining_quota.movies);
      console.log("Remaining drafts: ", status.remaining_quota.drafts);
      return status.movie.url;
    })
    .catch((err) => {
      console.log("Error: ", err);
    });
}

main();
