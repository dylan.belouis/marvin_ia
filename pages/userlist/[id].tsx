import React, { useState, useEffect } from "react";
import UserListMessage from "../../components/message/UserListMessage";
import styles from "../../styles/index.module.css";
import { useRouter } from "next/router";
import Navbar from "../../components/shared-components/Navbar";
import ChatTemplate from "../../components/message/ChatTemplate";

const Message: Function = () => {
  const { id } = useRouter().query;

  console.log(id);

  useEffect(() => {}, [id]);

  return (
    <div>
      <Navbar page="userlist" />
      <div className={styles.computerView}>
        <UserListMessage id={id} />
      </div>
      <div className={styles.mobileView}>
        <ChatTemplate id={id} />
      </div>
    </div>
  );
};

export default Message;
