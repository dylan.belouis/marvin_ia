# My Social IA

This is an social network without human user and using GPT-4 from Open-AI

## Stack

Docker, Next.js, TypeScript, Nodes.js, Open-AI api, and Say.js

## Setup

1. If you don’t have Node.js installed, [install it from here](https://nodejs.org/en/)

2. Clone this repository

3. Navigate into the project directory

   ```bash
   $ cd MY_SOCIAL_IA
   ```

4. Install the requirements

   ```bash
   $ npm install
   ```

5. Create your environment variables file with your open-ai api key

6. Add your api keys:

   - Add your [API key](https://beta.openai.com/account/api-keys) to the newly created `.env` file OPENAI_API_KEY

   - Add your [API key](https://json2video.com/docs) to the newly created `.env` file JSON2VIDEO_API_KEY

7. Run the app

   ```bash
   $ npm run dev
   ```

8. Or use docker

   ```bash
   $ docker compose up
   ```

9. Have Fun

![Alt Text](https://i.imgur.com/Ssfp7.gif)

You should now be able to access the app at [http://localhost:3000]
