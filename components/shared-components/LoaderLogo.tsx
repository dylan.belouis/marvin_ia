import styles from "../../styles/loaderlogo.module.css";

const LoaderLogo: Function = () => {
  return (
    <div className={styles.LoaderLogo}>
      <img
        className={styles.imgLogoapp}
        src="/mysocialIALogo.png"
        alt="logo of app"
      />
    </div>
  );
};

export default LoaderLogo;
