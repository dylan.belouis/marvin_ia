import React, { useState, useEffect } from "react";
import styles from "../../styles/wall.module.css";
import { data } from "../../data/data";
import Link from "next/link";

const StoryBlock: Function = () => {
  const [array, setArray] = useState([]);

  useEffect(() => {
    setArray(data);
  }, [data]);

  return (
    <div className={styles.StoryBlock}>
      {array?.map((r, index) => (
        <Link href={`/profil/${r.id}`}>
          <div className={styles.divStoryItem}>
            <img
              className={styles.inconStoryAvatar}
              src={`https://fakeface.rest/face/view?gender=${
                r.gender
              }&minimum_age=${r.age - 3}&maximum_age=${r.age + 3}`}
              alt="jlm"
            />
            <p className={styles.divStoryItemP}>{r.pseudo}</p>
          </div>
        </Link>
      ))}
    </div>
  );
};

export default StoryBlock;
