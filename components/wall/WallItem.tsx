import React, { useState, useEffect } from "react";
import styles from "../../styles/wall.module.css";
import Link from "next/link";
import propsWallPost from "../../interface/propsWallPost";
import WallItemToolbar from "./WallItemToolbar";
import { data } from "../../data/data";

const WallItem: Function = ({ r, index }: propsWallPost) => {
  const [postLike, setPostLike] = useState<boolean>(false);
  const postLikeNumber: number = Math.trunc(Math.random() * (data.length - 1));
  return (
    <div key={index} className={styles.Wall_div_post}>
      <Link href={`/profil/${r.id}`}>
        <div className={styles.divHeaderWall}>
          <div className={styles.divGeneralImageWall}>
            <img
              className={styles.icon_avatar_header}
              src={`https://fakeface.rest/face/view?gender=${
                r.gender
              }&minimum_age=${r.age - 3}&maximum_age=${r.age + 3}`}
              alt="jlm"
            />
            <p className={styles.Wall_p_textMessage}>{r.name}</p>
          </div>
          <svg
            aria-label="Plus d’options"
            className={styles.svgMoreIconButton}
            color="#262626"
            fill="#262626"
            height="24"
            role="img"
            viewBox="0 0 24 24"
            width="24"
          >
            <circle cx="12" cy="12" r="1.5"></circle>
            <circle cx="6" cy="12" r="1.5"></circle>
            <circle cx="18" cy="12" r="1.5"></circle>
          </svg>
        </div>
      </Link>
      <img
        key={index}
        src={`https://loremflickr.com/800/600/${
          r.hobbies[Math.trunc(Math.random() * (r.hobbies.length - 1))]
        }/any`}
        alt="post"
        className={styles.img_wall_profil}
      />
      <img
        key={index}
        src={`https://loremflickr.com/300/200/${
          r.hobbies[Math.trunc(Math.random() * (r.hobbies.length - 1))]
        }/any`}
        alt="post"
        className={styles.img_wall_profilMobile}
      />
      <div className={styles.divGeneralToolbarPost}>
        <WallItemToolbar
          id={r.id}
          postLike={postLike}
          setPostLike={setPostLike}
        />
        <div className={styles.divwallItemLikes}>
          <p className={styles.pwallItemLikes}>Aimé par&nbsp;</p>
          {postLike ? (
            <p className={styles.pwallItemLikesStrong}>vous</p>
          ) : (
            <p className={styles.pwallItemLikesStrong}>
              {data[Math.trunc(Math.random() * (data.length - 1))].name}
            </p>
          )}
          <p className={styles.pwallItemLikes}>&nbsp;et&nbsp;</p>
          <p className={styles.pwallItemLikesStrong}>
            {postLikeNumber}
            &nbsp;autres personnes
          </p>
        </div>
      </div>
    </div>
  );
};

export default WallItem;
