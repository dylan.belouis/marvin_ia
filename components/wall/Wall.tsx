import React, { useState, useEffect } from "react";
import styles from "../../styles/wall.module.css";
import { data } from "../../data/data";
import WallItem from "./WallItem";
import StoryBlock from "./StoryBlock";

const Wall: Function = () => {
  const [array, setArray] = useState([]);

  useEffect(() => {
    setArray(data);
  }, [data]);

  return (
    <div className={styles.Wall}>
      <h1>Mur</h1>
      <StoryBlock />
      <div className={styles.Wall_div_listPost}>
        {array?.map((r, index) => (
          <WallItem r={r} index={index} />
        ))}
      </div>
    </div>
  );
};

export default Wall;
