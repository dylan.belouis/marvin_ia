import { useRouter } from "next/router";
import { data } from "../../data/data";
import Link from "next/link";
import styles from "../../styles/profil.module.css";
import ProfilHeader from "./ProfilHeader";

const ProfilTemplate = () => {
  const profilId: any | number = useRouter().query.profilId;
  const dataIndex = data[profilId - 1];
  const interrestArray: string[] = dataIndex ? dataIndex.hobbies : [];
  return (
    <div className={styles.main}>
      <h1>Profil</h1>
      <ProfilHeader />
      <div className={styles.div_profil_wall}>
        {interrestArray.map((r, index) => {
          return (
            <div key={index}>
              <img
                src={`https://loremflickr.com/800/600/${r}/any`}
                alt="post"
                className={styles.img_wall_profil}
              />
            </div>
          );
        })}
      </div>
      <div className={styles.div_profil_wallMobile}>
        {interrestArray.map((r, index) => {
          return (
            <div key={index} className={styles.divStylePostProfil}>
              <img
                src={`https://loremflickr.com/800/600/${r}/any`}
                alt="post"
                className={styles.img_wall_profil}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default ProfilTemplate;
