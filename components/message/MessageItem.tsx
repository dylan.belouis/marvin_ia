import React, { useState, useEffect } from "react";
import styles from "../../styles/messageItem.module.css";
import { useRouter } from "next/router";
import { data } from "../../data/data";
import userInterface from "../../interface/userInterface";
import propsData from "../../interface/propsData";

const MessageItem: Function = ({ r, index, array }: propsData) => {
  const { id }: any | number = useRouter().query;
  const fakeDatabase: Array<userInterface> = data;
  const dataIndex: userInterface = fakeDatabase[id - 1];
  const [imgUrl, setImageUrl] = useState<string>("");

  useEffect(() => {
    if (dataIndex && dataIndex.gender)
      setImageUrl(
        `https://fakeface.rest/face/view?gender=${
          dataIndex.gender
        }&minimum_age=${dataIndex.age - 3}&maximum_age=${dataIndex.age + 3}`
      );
  }, [dataIndex]);

  return (
    <div key={`MessageItem${index}`} className={styles.responseData}>
      {r.sender === "me" && (
        <div className={styles.divGeneralMessage}>
          <div className={styles.divDisplayMyMessage}>
            <div className={styles.textSend}>
              <p className={styles.header_text}>{r.message}</p>
            </div>
          </div>
          {index < array.length - 1 && array[index + 1].sender !== "me" && (
            <p
              className={styles.p_hour_messageItem}
            >{`${r.hours}:${r.minutes}`}</p>
          )}
        </div>
      )}
      {r.sender === "IA" && (
        <div className={styles.divGeneralMessage}>
          {dataIndex.response_type === "image" && (
            <img className={styles.icon_avatar} src={imgUrl} alt="dog" />
          )}
          <div className={styles.divDisplayIAMessage}>
            {dataIndex.response_type === "message" ? (
              <img className={styles.icon_avatar} src={imgUrl} alt="dog" />
            ) : (
              dataIndex.response_type === "special" && (
                <img className={styles.icon_avatar} src={imgUrl} alt="dog" />
              )
            )}
            {dataIndex.response_type === "message" ||
            dataIndex.response_type === "special" ? (
              <div className={styles.textReceived}>
                <p>{r.message}</p>
              </div>
            ) : (
              <div className={styles.textReceivedImage}>
                <img
                  className={styles.imageGenerate}
                  src={r.message}
                  alt="Il y a un probleme avec l'hebergement"
                />
              </div>
            )}
          </div>
          {index < array.length - 1 && array[index + 1].sender !== "IA" && (
            <p
              className={styles.p_hour_messageItem}
            >{`${r.hours}:${r.minutes}`}</p>
          )}
        </div>
      )}
    </div>
  );
};

export default MessageItem;
