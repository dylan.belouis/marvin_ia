import React, { useState, useEffect } from "react";
import { data } from "../../data/data";
import styles from "../../styles/userlist.module.css";
import ChatTemplate from "./ChatTemplate";
import Link from "next/link";
import Image from "next/image";

const UserList: Function = ({ id }) => {
  const [array, setArray] = useState([]);

  useEffect(() => {
    setArray(data);
  }, []);

  return (
    <div
      className={styles.UserList}
      style={{
        width: "100vw",
        minWidth: "100vw",
      }}
    >
      <div className={styles.UserList_div_block}>
        <div className={styles.UserList_div_halfBlock}>
          <div className={styles.UserList_div_name}>
            <p className={styles.UserList_p_name}>user_name</p>
            <div className={styles.UserList_div_chevron}>
              <svg
                aria-label="Icône chevron vers le bas"
                className="_ab6-"
                color="#262626"
                fill="#262626"
                height="20"
                role="img"
                viewBox="0 0 24 24"
                width="20"
              >
                <path d="M21 17.502a.997.997 0 01-.707-.293L12 8.913l-8.293 8.296a1 1 0 11-1.414-1.414l9-9.004a1.03 1.03 0 011.414 0l9 9.004A1 1 0 0121 17.502z"></path>
              </svg>
            </div>
          </div>
          <div className={styles.UserList_div_blockMessage}>
            <h1 className={styles.UserList_title}>Messages</h1>
            <div className={styles.UserList_div_blockListMessage}>
              {array &&
                array.map(
                  (
                    r: {
                      gender: string;
                      age: number;
                      name: string;
                      id: number;
                    },
                    index: number
                  ) => {
                    return (
                      <Link key={index} href={`/userlist/${r.id}`}>
                        <div className={styles.UserList_div_peopleItem}>
                          <div className={styles.UserList_div_image}>
                            <img
                              className={styles.icon_avatar_header}
                              src={`https://fakeface.rest/face/view?gender=${
                                r.gender
                              }&minimum_age=${r.age - 3}&maximum_age=${
                                r.age + 3
                              }`}
                              alt="jlm"
                            />
                          </div>
                          <div className={styles.UserList_div_textMessage}>
                            <p className={styles.UserList_p_textMessage}>
                              {r.name}
                            </p>
                            <span
                              className={
                                styles.UserList_p_textMessage_secondary
                              }
                            >
                              Ecrivez un message..&nbsp;&nbsp;&nbsp;.&nbsp;1 sem
                            </span>
                          </div>
                        </div>
                      </Link>
                    );
                  }
                )}
            </div>
          </div>
        </div>
        <div className={styles.UserList_div_halfBlock3}>
          <ChatTemplate id={id} />
        </div>
      </div>
    </div>
  );
};

export default UserList;
