import React, {
  useState,
  useEffect,
  useRef,
  ReactComponentElement,
} from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { data } from "../../data/data";
import MessageItem from "./MessageItem";
import styles from "../../styles/ChatTemplate.module.css";
import userInterface from "../../interface/userInterface";

const ChatTemplate: Function = () => {
  const messagesEndRef = useRef(null);
  const [message, setMessage] = useState<string>("");
  const [array, setArray] = useState([]);
  const typedData: Array<userInterface> = data;
  const id: any | string[] | Number = useRouter().query?.id;
  const dataIndex: userInterface = typedData[id - 1];
  const [imgUrl, setImageUrl] = useState<string>("");
  const [previusId, setPreviusId] = useState<number>(id ? id : 0);

  useEffect(() => {
    if (id !== previusId) setPreviusId(id);
    setArray([]);
    if (dataIndex)
      dataIndex.gender &&
        setImageUrl(
          `https://fakeface.rest/face/view?gender=${
            dataIndex.gender
          }&minimum_age=${dataIndex.age - 3}&maximum_age=${dataIndex.age + 3}`
        );
  }, [dataIndex, id]);

  async function onSubmit(event) {
    event.preventDefault();
    await setMessage("");
    await setArray((oldArray) => [
      ...oldArray,
      {
        message: message,
        hours: new Date().getHours(),
        minutes: new Date().getMinutes(),
        sender: "me",
      },
    ]);
    const response = await fetch(`/api/${dataIndex.api_link}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        message: message,
        prompt: dataIndex.prompt,
        dataIndex: dataIndex,
      }),
    });

    const data = await response.json();
    const objectSendResponse = {
      message: data.result,
      hours: new Date().getHours(),
      minutes: new Date().getMinutes(),
      sender: "IA",
    };
    await setArray((oldArray) => [...oldArray, objectSendResponse]);
    window.scrollTo({
      top: 10000,
      behavior: "smooth",
    });
  }

  return (
    <div ref={messagesEndRef} id="chatTemplate" className={styles.main}>
      <Link href={`/profil/${id}`}>
        <div className={styles.header}>
          <img className={styles.icon_avatar_header} src={imgUrl} alt="jlm" />
          <h3 className={styles.h3ChatTemplate}>{dataIndex?.name}</h3>
        </div>
      </Link>
      <div className={styles.result}>
        {dataIndex?.messages?.map((r, index) => (
          <div key={index}>
            <MessageItem r={r} index={index} array={dataIndex.messages} />
          </div>
        ))}
        {array[0] &&
          array.map((r, index) => (
            <div key={index}>
              <MessageItem r={r} index={index} array={array} />
            </div>
          ))}
      </div>
      <form className={styles.form} onSubmit={onSubmit}>
        <div className={styles.div_form_Message}>
          <input
            className={styles.input}
            type="text"
            name="animal"
            autoComplete="off"
            placeholder="Votre message..."
            value={message}
            onChange={(e) => setMessage(e.target.value)}
          />
          <p onClick={(e) => onSubmit(e)} className={styles.sendButton}>
            Envoyer
          </p>
        </div>
      </form>
    </div>
  );
};

export default ChatTemplate;
