export default interface PropsData {
  r: { message: string; minutes: number; hours: number; sender: string };
  index: number;
  array: [{ message: string; minutes: number; hours: number; sender: string }];
}
