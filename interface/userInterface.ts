export default interface user {
  id: number;
  name: string;
  description: Array<string>;
  pseudo: string;
  prompt: string;
  model: string;
  messageName: string;
  temperature: number;
  max_tokens: number;
  top_p: number;
  frequency_penalty: number;
  presence_penalty: number;
  hobbies: Array<string>;
  messages: {
    message: string;
    minutes: number;
    hours: number;
    sender: string;
  }[];
  gender: string;
  Follower: number;
  age: number;
  stop?: Array<string>;
  response_type: string;
  api_link?: string;
}
