export default interface propsWallPost {
  r: {
    id: number;
    name: string;
    description: Array<string>;
    pseudo: string;
    prompt: string;
    messageName: string;
    model: string;
    temperature: number;
    max_tokens: number;
    top_p: number;
    frequency_penalty: number;
    presence_penalty: number;
    hobbies: Array<string>;
    messages: {
      message: string;
      minutes: number;
      hours: number;
      sender: string;
    }[];
    gender: string;
    Follower: number;
    age: number;
    stop?: Array<string>;
    response_type: string;
    api_link?: string;
  };
  index: number;
}
